<?php

namespace Drupal\visual_website_optimizer\EventSubscriber;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Render\AttachmentsInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserDataInterface;
use Drupal\visual_website_optimizer\Render\VwoSmartCodeMarkup;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Adds VWO Smart Code to page if configured to do so.
 */
class VwoSmartCodeEventSubscriber implements EventSubscriberInterface {

  /**
   * The Configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Retrieves the currently active route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The current path for the current request.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPathStack;

  /**
   * Provides a path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * User data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * Module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Flag if the SmartCode should be included in the response.
   *
   * @var bool|null
   */
  protected $flagAdd = NULL;

  /**
   * Custom URL to include in JS.
   *
   * @var string
   */
  protected $customUrl = '';

  /**
   * Flag to indicate that JS has already been added.
   *
   * @var bool
   */
  protected $added = FALSE;

  /**
   * Cache contexts to include in response.
   *
   * @var array
   */
  protected $cacheContexts = [];

  /**
   * Create a new VwoSmartCodeEventSubscriber object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Retrieves the currently active route match object.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user account.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path_stack
   *   The current path for the current request.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   Provides a path matcher.
   * @param \Drupal\user\UserDataInterface $user_data
   *   User data service.
   *
   * @phpcs:disable Drupal.Functions.MultiLineFunctionDeclaration.MissingTrailingComma
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    RouteMatchInterface $route_match,
    AccountProxyInterface $current_user,
    ModuleHandlerInterface $module_handler,
    CurrentPathStack $current_path_stack,
    PathMatcherInterface $path_matcher,
    UserDataInterface $user_data
  ) {
    // phpcs:enable
    $this->configFactory = $config_factory;
    $this->routeMatch = $route_match;
    $this->currentUser = $current_user;
    $this->moduleHandler = $module_handler;
    $this->currentPathStack = $current_path_stack;
    $this->pathMatcher = $path_matcher;
    $this->userData = $user_data;

    $this->config = $this->configFactory
      ->get('visual_website_optimizer.settings');
  }

  /**
   * Flag that the JS should be included in this response.
   *
   * @param string|null $custom_url
   *   The valid custom url to include in the javascript. Set to '' to clear.
   *
   * @return bool
   *   Return FALSE if trying to set custom URL and it was invalid.
   *
   * @throws \Exception
   *   If method is called after KernelEvents::RESPONSE already triggered.
   */
  public function includeJs(?string $custom_url = NULL) {

    // If this is run out of order, fatal call as this is a developer.
    if ($this->added) {
      throw new \Exception('Hook visual_website_optimizer_page_attachments already run.');
    }

    // Update the custom URL if different.
    if (!is_null($custom_url)) {
      if (!empty($custom_url) && !UrlHelper::isValid($custom_url, TRUE)) {
        return FALSE;
      }
      $this->customUrl = $custom_url;
    }

    return $this->flagAdd = TRUE;
  }

  /**
   * Flag that JS should NOT be included in this response.
   *
   * @throws \Exception
   *   If method is called after KernelEvents::RESPONSE already triggered.
   */
  public function disable() {
    // Check to see if listener has already executed.
    if ($this->added) {
      throw new \Exception('Hook visual_website_optimizer_page_attachments already run.');
    }

    // Explicitly disable.
    $this->flagAdd = FALSE;
  }

  /**
   * Set the CacheableMetadata included in the response.
   *
   * Primary used when added to page using self::includeJs().
   *
   * @param array $cache_contexts
   *   An array of CacheableMetadata to include in response.
   */
  public function setCacheableDependencies(array $cache_contexts) {
    $this->cacheContexts = $cache_contexts;
  }

  /**
   * Event handler for KernelEvents::RESPONSE.
   */
  public function onResponse(ResponseEvent $event) {
    // Do not proceed if already processed.
    if ($this->added) {
      return;
    }

    // Do not proceed if explicitly disabled for this response.
    if (!is_null($this->flagAdd) && !$this->flagAdd) {
      return;
    }

    // Only act upon the master request and not sub-requests.
    // @todo Only use "isMainRequest()" here, once Drupal 9 and prior support
    // is dropped.
    $isMainRequest = version_compare(\Drupal::VERSION, '10', '<')
      ? $event->isMasterRequest()
      : $event->isMainRequest();
    if (!$isMainRequest) {
      return;
    }

    // This is the main request running this method. Flag as added to save
    // checking again.
    $this->added = TRUE;

    $response = $event->getResponse();
    // Only act if the response is one that is able to have attachments.
    if (!($response instanceof AttachmentsInterface)) {
      return;
    }

    // If not configured, no point to continue.
    $id = $this->config->get('id');
    if ($id == NULL) {
      return;
    }

    // Do not proceed if not flagged to add, and filtering not enabled.
    $filter = ($this->config->get('filter.enabled') == 'on');
    if (!$filter && !$this->flagAdd) {
      return;
    }

    // Check filter and add appropriate cache contexts. Don't run filters if
    // already flagged to add.
    if (!$this->flagAdd && $filter) {
      // Run filters, settings self::addJs and self::cacheContexts.
      $this->filterAllows();
    }

    // Add the javascript to the attachment.
    if ($this->flagAdd) {

      // Get any existing attachments.
      $attachments = $response->getAttachments();

      // Synchronous implementation.
      if ($this->config->get('loading.type') == 'sync') {
        $attachments['html_head'][] = [
          [
            '#type' => 'html_tag',
            '#tag' => 'script',
            '#value' => '',
            '#attributes' => [
              'src' => "https://dev.visualwebsiteoptimizer.com/lib/{$id}.js",
            ],
          ],
          'visual_website_optimizer',
        ];

        // Set the updated array back on the object.
        $response->setAttachments($attachments);

        // Do not alter cache contexts.
        return;
      }

      // Asynchronous implementation.
      $settings = [
        'id' => $id,
        'timeout_library' => $this->config->get('loading.timeout.library'),
        'timeout_setting' => $this->config->get('loading.timeout.settings'),
        'usejquery' => $this->config->get('loading.usejquery'),
        'testnull' => NULL,
      ];
      if (!empty($this->customUrl)) {
        $settings['custom_url'] = $this->customUrl;
      }
      elseif (!empty($this->config->get('loading.custom_url'))) {
        $settings['custom_url'] = $this->config->get('loading.custom_url');
      }

      // Add preconnect. Use 'html_head' to keep in correct position.
      $attachments['html_head'][] = [
        [
          '#type' => 'html_tag',
          '#tag' => 'link',
          '#value' => '',
          '#attributes' => [
            'rel' => 'preconnect',
            'href' => 'https://dev.visualwebsiteoptimizer.com',
          ],
        ],
        'visual_website_optimizer_preconnect',
      ];

      // Use VwoSmartCodeMarkup to output a non-escaped script to headers.
      $attachments['html_head'][] = [
        [
          '#type' => 'html_tag',
          '#tag' => 'script',
          '#value' => VwoSmartCodeMarkup::createFromArray($settings),
          '#attributes' => [
            'type' => 'text/javascript',
            'id' => 'vwoCode',
          ],
        ],
        'visual_website_optimizer',
      ];

      // Set the updated array back on the object.
      $response->setAttachments($attachments);
    }

    // Add the cache contexts if set and possible.
    if (!empty($this->cacheContexts) && $response instanceof CacheableResponseInterface) {
      // Add each cache contexts to the response.
      foreach ($this->cacheContexts as $cachemeta) {
        $response->addCacheableDependency($cachemeta);
      }
    }
  }

  /**
   * Filter based on configured visibility settings.
   *
   * @return bool
   *   Returns TRUE if should be included, or FALSE if not.
   */
  protected function filterAllows() {

    // Assumption is that we add the code and negate with tests.
    $add = TRUE;

    // Per user opt-out.
    $usercontrol = $this->config->get('filter.userconfig');
    if ($usercontrol != 'nocontrol' && $this->currentUser->id()) {

      // Start with default.
      $addjs = ($usercontrol == 'optin') ? FALSE : TRUE;

      // Get user data and set if needed.
      $userconfig = $this->userData->get(
        'visual_website_optimizer',
        $this->currentUser->id(),
        'userconfig'
      );
      if (isset($userconfig)) {
        $addjs = $userconfig;
      }

      // Add the caching context and indicate choice.
      $add = (bool) $addjs;
      $this->cacheContexts['user'] = CacheableMetadata::createFromObject(
        $this->currentUser
      );
    }

    // Node type filtering.
    if ($add && $include_node_types = $this->config->get('filter.nodetypes')) {
      $node = $this->routeMatch->getParameter('node');

      if ($node) {
        $add = in_array($node->getType(), $include_node_types);
        $this->cacheContexts['node'] = CacheableMetadata::createFromObject(
          $node
        );
      }

      else {
        // Condition requires that the page be a node based one.
        $add = FALSE;
      }
    }

    // Role filtering.
    if ($add && $include_roles = $this->config->get('filter.roles')) {
      $intersect = array_intersect(
        $include_roles,
        $this->currentUser->getRoles()
      );

      $add = ($intersect) ? TRUE : FALSE;
      $meta = new CacheableMetadata();
      $meta->setCacheContexts(['user.permissions']);
      $this->cacheContexts['roles'] = $meta;
    }

    // Path filtering.
    if ($add && $pathlist = $this->config->get('filter.page.list')) {
      $filter_type = $this->config->get('filter.page.type');

      // Eval the PHP code.
      if ($filter_type == 'usephp') {
        // Only actually run if the php module is also here.
        // See https://www.drupal.org/node/2088811
        if ($this->moduleHandler->moduleExists('php')) {
          if (php_eval($pathlist)) {
            $add = TRUE;
          }
          else {
            $add = FALSE;
          }
          // @todo Cache context? Currently inherits the page filter context
          // of url.path set below.
        }
      }

      // Check against the path and alias.
      else {
        $current_path = $this->currentPathStack->getPath();

        // @todo API update probable: Change PathMatcher::matchPath()
        // $patterns param from string to an array of strings.
        // @see https://www.drupal.org/node/2274701
        $matched = $this->pathMatcher->matchPath($current_path, $pathlist);

        // If we haven't matched, also check against the alias.
        if (!$matched) {
          // Get the alias. The path_alias module is optional from 9.0.0. If
          // it is not enabled, just skip the test.
          // @see https://www.drupal.org/node/3092086
          try {
            /** @var \Drupal\path_alias\AliasManagerInterface $path_alias_manager */
            // @phpcs:ignore
            $path_alias_manager = \Drupal::service('path_alias.manager');

            $alias = $path_alias_manager->getAliasByPath($current_path);

            // Only check again if there's a difference.
            if ($current_path != $alias) {
              $matched = $this->pathMatcher->matchPath($alias, $pathlist);
            }
          }
          catch (ServiceNotFoundException $e) {
            // NOOP.
            $path_alias_manager = FALSE;
          }
        }

        if ($filter_type == 'listexclude' && $matched) {
          $add = FALSE;
        }
        if ($filter_type == 'listinclude' && !$matched) {
          $add = FALSE;
        }
      }

      // Add cache context.
      $meta = new CacheableMetadata();
      $meta->setCacheContexts(['url.path']);
      $this->cacheContexts['paths'] = $meta;
    }

    // Update and return if the Js should be added.
    return $this->flagAdd = $add;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      // Respond to KernelEvents::RESPONSE.
      KernelEvents::RESPONSE => ['onResponse', 1000],
    ];
  }

}
