<?php

namespace Drupal\visual_website_optimizer\Render;

use Drupal\Component\Render\MarkupInterface;

/**
 * Render VWO Smart Code javascript without the result being escaped.
 */
class VwoSmartCodeMarkup implements MarkupInterface {

  /**
   * The safe string.
   *
   * @var string
   */
  protected $string;

  /**
   * Create a new VwoSmartCodeMarkup and return it.
   *
   * @param array $settings
   *   Settings used to customize the output.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The VwoSmartCodeMarkup instance.
   */
  public static function createFromArray(array $settings = []) {
    if (!isset($settings['id']) || empty($settings['id'])) {
      return '';
    }

    $safe_string = new static();
    $use_jquery = ($settings['usejquery'] == 'local') ? 'true' : 'false';

    // Setup for custom_url.
    $uri_variable = 'd.URL';
    $custom_url_var = '';
    if (isset($settings['custom_url']) && !empty($settings['custom_url'])) {
      $uri_variable = '_vis_opt_url';
      $custom_url_var = "\n    _vis_opt_url = '{$settings['custom_url']}',";
    }

    // The original VWO 2.1 code modified to take custom url and lib variables.
    $safe_string->string = implode('', [
      "window._vwo_code || (function () {",
      "var account_id = {$settings['id']},{$custom_url_var}",
      "version = 2.1,",
      "settings_tolerance = {$settings['timeout_setting']},",
      "library_tolerance = {$settings['timeout_library']},",
      "use_existing_jquery = {$use_jquery},",
      "hide_element = 'body',",
      "hide_element_style = 'opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;transition:none !important;',",
      // The remainder is the original js wrapped at 70 characters.
      // cspell:disable
      "/* DO NOT EDIT BELOW THIS LINE */",
      "f=false,w=window,d=document,v=d.querySelector('#vwoCode'),cK='_vwo_'+a",
      "ccount_id+'_settings',cc={};try{var c=JSON.parse(localStorage.getItem(",
      "'_vwo_'+account_id+'_config'));cc=c&&typeof c==='object'?c:{}}catch(e)",
      "{}var stT=cc.stT==='session'?w.sessionStorage:w.localStorage;code={non",
      "ce:v&&v.nonce,use_existing_jquery:function(){return typeof use_existin",
      "g_jquery!=='undefined'?use_existing_jquery:undefined},library_toleranc",
      "e:function(){return typeof library_tolerance!=='undefined'?library_tol",
      "erance:undefined},settings_tolerance:function(){return cc.sT||settings",
      "_tolerance},hide_element_style:function(){return'{'+(cc.hES||hide_elem",
      "ent_style)+'}'},hide_element:function(){if(performance.getEntriesByNam",
      "e('first-contentful-paint')[0]){return''}return typeof cc.hE==='string",
      "'?cc.hE:hide_element},getVersion:function(){return version},finish:fun",
      "ction(e){if(!f){f=true;var t=d.getElementById('_vis_opt_path_hides');i",
      "f(t)t.parentNode.removeChild(t);if(e)(new Image).src='https://dev.visu",
      "alwebsiteoptimizer.com/ee.gif?a='+account_id+e}},finished:function(){r",
      "eturn f},addScript:function(e){var t=d.createElement('script');t.type=",
      "'text/javascript';if(e.src){t.src=e.src}else{t.text=e.text}v&&t.setAtt",
      "ribute('nonce',v.nonce);d.getElementsByTagName('head')[0].appendChild(",
      "t)},load:function(e,t){var n=this.getSettings(),i=d.createElement('scr",
      "ipt'),r=this;t=t||{};if(n){i.textContent=n;d.getElementsByTagName('hea",
      "d')[0].appendChild(i);if(!w.VWO||VWO.caE){stT.removeItem(cK);r.load(e)",
      "}}else{var o=new XMLHttpRequest;o.open('GET',e,true);o.withCredentials",
      "=!t.dSC;o.responseType=t.responseType||'text';o.onload=function(){if(t",
      ".onloadCb){return t.onloadCb(o,e)}if(o.status===200||o.status===304){_",
      "vwo_code.addScript({text:o.responseText})}else{_vwo_code.finish('&e=lo",
      "ading_failure:'+e)}};o.onerror=function(){if(t.onerrorCb){return t.one",
      "rrorCb(e)}_vwo_code.finish('&e=loading_failure:'+e)};o.send()}},getSet",
      "tings:function(){try{var e=stT.getItem(cK);if(!e){return}e=JSON.parse(",
      "e);if(Date.now()>e.e){stT.removeItem(cK);return}return e.s}catch(e){re",
      "turn}},init:function(){if({$uri_variable}.indexOf('__vwo_disable__')>-",
      "1)return;var e=this.settings_tolerance();w._vwo_settings_timer=setTime",
      "out(function(){_vwo_code.finish();stT.removeItem(cK)},e);var t;if(this",
      ".hide_element()!=='body'){t=d.createElement('style');var n=this.hide_e",
      "lement(),i=n?n+this.hide_element_style():'',r=d.getElementsByTagName('",
      "head')[0];t.setAttribute('id','_vis_opt_path_hides');v&&t.setAttribute",
      "('nonce',v.nonce);t.setAttribute('type','text/css');if(t.styleSheet)t.",
      "styleSheet.cssText=i;else t.appendChild(d.createTextNode(i));r.appendC",
      "hild(t)}else{t=d.getElementsByTagName('head')[0];var i=d.createElement",
      "('div');i.style.cssText='z-index: 2147483647 !important;position: fixe",
      "d !important;left: 0 !important;top: 0 !important;width: 100% !importa",
      "nt;height: 100% !important;background: white !important;display: block",
      " !important;';i.setAttribute('id','_vis_opt_path_hides');i.classList.a",
      "dd('_vis_hide_layer');t.parentNode.insertBefore(i,t.nextSibling)}var s",
      "='https://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+en",
      "codeURIComponent({$uri_variable})+'&vn='+version;if(w.location.search.",
      "indexOf('_vwo_xhr')!==-1){this.addScript({src:s})}else{this.load(s+'&x",
      "=true')}}};w._vwo_code=code;code.init();})();",
    ]);
    /* cspell:enable */

    return $safe_string;
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return $this->string;
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): mixed {
    // Part of interface but not implemented.
    return '';
  }

}
