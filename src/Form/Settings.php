<?php

namespace Drupal\visual_website_optimizer\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * VWO Settings form.
 */
class Settings extends FormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'visual_website_optimizer_settings';
  }

  /**
   * Create a new Settings form.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('module_handler'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('visual_website_optimizer.settings');

    $id = $config->get('id');
    $form['id_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Account'),
    ];

    // Add description to fieldset.
    if (empty($id) || $id == 'NONE') {
      // Get affiliate links from help so they remain in one place.
      $this->moduleHandler->loadInclude(
        'visual_website_optimizer',
        'inc',
        'visual_website_optimizer.help'
      );
      [$vwo_url_partner, $vwo_url_nopartner]
        = _visual_website_optimizer_help_links();

      $desc[] = '<p>';
      $desc[] = $this->t('In order to use this module, you will need a Visual Website Optimizer account. A Free 30 day trial account is available from the VWO website by clicking on either of the links below. To contribute towards the continued development of this module for free and with no effort, click on the affiliate link. Otherwise, click the other link without the affiliate marker.');
      $desc[] = '</p><p>';
      $desc[] = $this->t('<a href=":affiliate_url">Sign up for a Free 30 day trial account AND help out</a>, or <a href=":nude_url">Sign up for a Free 30 day trial without affiliate link.</a>', [
        ':affiliate_url' => $vwo_url_partner->toString(),
        ':nude_url' => $vwo_url_nopartner->toString(),
      ]);
      $desc[] = '</p>';

      $form['id_fieldset']['description'] = [
        '#type' => 'markup',
        '#markup' => implode('', $desc),
      ];
    }

    // Setup description.
    $vwoid_link = Url::fromRoute('visual_website_optimizer.settings.vwoid');
    $id_description = [
      $this->t('Your numeric Account ID or placeholder "NONE" to disable. This is the number after <q>var _vis_opt_account_id =</q> in the VWO Smart Code.'),
      '<br /><strong>',
      $this->t('You can use the <a href=":url">Parse Account ID</a> tool to extract the Account ID from the full VWO Smart Code.', [
        ':url' => $vwoid_link->toString(),
      ]),
      '</strong>',
    ];

    $form['id_fieldset']['id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('VWO Account ID'),
      '#description' => implode('', $id_description),
      '#size' => 15,
      '#maxlength' => 20,
      '#required' => TRUE,
      '#default_value' => ($id == NULL) ? 'NONE' : $id,
    ];

    $form['synchtype_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('(A)Synchronous loading'),
    ];
    $form['synchtype_fieldset']['synchtype'] = [
      '#type' => 'radios',
      '#title' => $this->t('Javascript loading method'),
      '#description' => implode('', [
        '<p>',
        $this->t('Asynchronous loading is now the default. Please see <a target="_blank" href="https://vwo.com/blog/asynchronous-code/">https://vwo.com/blog/asynchronous-code/</a> for more details.'),
        '</p><p>',
        $this->t('The Asynchronous version of Visual Website Optimizer code reduces page load time as the VWO code is downloaded in parallel to site code. It also ensures that your site is never slowed down even if the VWO servers are inaccessible.'),
        '</p><p>',
        $this->t('VWO have extensively tested the asynchronous code across variety of browsers (including IE7) and it works perfectly.'),
        '</p>',
      ]),
      '#options' => [
        'async' => $this->t('Asynchronous (default)'),
        'sync' => $this->t('Synchronous'),
      ],
      '#required' => TRUE,
      '#default_value' => $config->get('loading.type'),
    ];

    $form['advanced'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this->t('Advanced Asynchronous Settings'),
      '#description' => implode('', [
        '<p>',
        $this->t('These settings are only used when Asynchronous loading mode is selected above.'),
        '</p><p>',
        $this->t('Note that in normal circumstances, all the data and files that need to be download will get downloaded in 100-200 milliseconds, so the following timeouts are an absolute maximum threshold and can safely be kept as is.'),
        '</p><p>',
        $this->t('One possible side effect of decreasing these timeouts, would be users on slower internet connections reaching the timeout and as a result, missing out on becoming part of the test.'),
        '</p>',
      ]),
      '#states' => [
        'visible' => [
          ':input[name=synchtype]' => ['value' => 'async'],
        ],
      ],
    ];

    $form['advanced']['asynctollibrary'] = [
      '#type' => 'number',
      '#title' => $this->t('Test Library Download Timeout'),
      '#description' => $this->t('The maximum time in milliseconds the code snippet will wait for the VWO javascript library to be downloaded from the Amazon Cloudfront Content Delivery Network. If the library is not available in this time, your original page will be displayed without tests. Default: 1500 ms.'),
      '#size' => 10,
      '#min' => 0,
      '#max' => 9999,
      '#required' => TRUE,
      '#default_value' => $config->get('loading.timeout.library'),
    ];

    $form['advanced']['asynctolsettings'] = [
      '#type' => 'number',
      '#title' => $this->t('Test Settings Download Timeout'),
      '#description' => $this->t('The maximum time in milliseconds the code snippet will wait for test settings to arrive from the VWO servers. If no settings arrive within this period, your original page will be displayed without tests. Default: 2000 ms.'),
      '#size' => 10,
      '#min' => 0,
      '#max' => 9999,
      '#required' => TRUE,
      '#default_value' => $config->get('loading.timeout.settings'),
    ];

    $form['advanced']['asyncusejquery'] = [
      '#type' => 'radios',
      '#title' => $this->t('Use existing jQuery'),
      '#description' => $this->t('Configure the "use_existing_jquery" option in the code snippet. Please provide feedback to the module Author regarding your experiences with this setting.'),
      '#options' => [
        'local' => $this->t('True'),
        'import' => $this->t('False (default)'),
      ],
      '#required' => TRUE,
      '#default_value' => $config->get('loading.usejquery'),
    ];

    $form['cust_url'] = [
      '#type' => 'details',
      '#open' => !empty($config->get('loading.custom_url')),
      '#title' => $this->t('Custom URL'),
      '#description' => $this->t('When VWO code runs on a given web page, it usually uses the URL of the page viewed to trigger configured A/B and multivariate tests. With a Custom URL, the site can report one, possibly fake, URL for VWO side URL matching instead. This is a global value, so all pages matched by local filters will use this URL.<br />For more nuanced filtering or to use more than one Custom URL, use a custom module and <code>visual_website_optimizer_include_js()</code>.'),

      'custom_url' => [
        '#type' => 'textfield',
        '#title' => $this->t('URL'),
        '#description' => $this->t('A fully qualified custom URL used to replace the current page URL submitted in SmartCode, or empty for default.'),
        '#default_value' => $config->get('loading.custom_url'),
      ],

      '#states' => [
        'visible' => [
          ':input[name=synchtype]' => ['value' => 'async'],
        ],
      ],
    ];

    $form['flush'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Flush caches on save'),
      '#description' => $this->t('Changes to these settings may not be reflected on the site until caches are flushed.'),
      '#default_value' => FALSE,
    ];

    $form['actions'] = [
      '#type' => 'actions',

      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save configuration'),
        '#button_type' => 'primary',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // VWO ID Was not set to "number" field to allow for setting it to "NONE",
    // and so much be manually validated.
    $vwoid = $form_state->getValue('id');
    if (!preg_match('/^\d+$/', $vwoid) && $vwoid != 'NONE') {
      $vwoid_link = Url::fromRoute('visual_website_optimizer.settings.vwoid');
      $form_state->setErrorByName('id',
        $this->t('Your Visual Website Optimizer ID must be numeric (or set to "NONE" to disable). If you have having issues locating it, please use the <a href=":parse_url">Extract Account Id tool</a>.', [
          ':parse_url' => $vwoid_link->toString(),
        ])
      );
    }

    // Custom URL must be a fully qualified valid URL.
    $custom_url = $form_state->getValue('custom_url');
    if (!empty($custom_url) && !UrlHelper::isValid($custom_url, TRUE)) {
      $form_state->setErrorByName('custom_url', $this->t(
        'Custom URL must be valid and fully qualified.'
      ));
    }

    // Translate that NONE into NULL for Config API.
    if ($vwoid == 'NONE') {
      $form_state->setValue('id', NULL);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Grab the editable configuration.
    $config = $this->configFactory()
      ->getEditable('visual_website_optimizer.settings');

    // Set each of the configuration values.
    $field_key_config_map = [
      'id' => 'id',
      'synchtype' => 'loading.type',
      'asynctollibrary' => 'loading.timeout.library',
      'asynctolsettings' => 'loading.timeout.settings',
      'asyncusejquery' => 'loading.usejquery',
      'custom_url' => 'loading.custom_url',
    ];
    foreach ($field_key_config_map as $field_key => $config_key) {
      $config->set($config_key, $form_state->getValue($field_key));
    }

    // Commit saved configuration.
    $config->save();

    $this->messenger()->addMessage($this->t(
      'Visual Website Optimizer settings have been saved.'
    ));

    // Flush all caches.
    if ($form_state->getValue('flush')) {
      $this->messenger()->addMessage($this->t(
        'Caches flushed.'
      ));
      drupal_flush_all_caches();
    }
  }

}
