# cspell: project specific dictionary

# Coding words and variable names which do not need to, or cannotbe changed.
addjs
asynctollibrary
asynctolsettings
asyncusejquery
cachemeta
cust
listexclude
listinclude
nocontrol
nodechecks
nodefilter
nodetypes
nopartner
optout
pagefilter
pagelist
pageradios
pathlist
rolechecks
rolefitler
smartcode
synchtype
testnull
usejquery
usephp
userconfig
usercontrol
userfilter
userradios
vwoid
yoursite

# Words which are words but which aren't in the CSpell dictionary.
optimiser

# Nouns.
Ronco
