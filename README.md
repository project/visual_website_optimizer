# Visual Website Optimizer module for Drupal

The Visual Website Optimizer module automatically includes the VWO javascript
in your site pages. [Visual Website Optimizer][1] is an easy to use A/B split,
and multivariate testing tool. VWO uses small snippets of javascript inserted
into the head of each page to perform its tests; the Visual Website Optimizer
module for Drupal automates the configuration and inclusion of those snippets.

The Visual Website Optimizer module requires an active VWO account. After
installing the module, you need to configure it for use with your VWO account
by entering your Account ID via settings page. Once configured, Visual Website
Optimizer tests will be automatically included in every page, until you
deactivate or disable the module.

[1]: https://vwo.com/

### Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules][2].

[2]: https://www.drupal.org/docs/extending-drupal/installing-drupal-modules


### Post-installation

1. Sign up for Visual Website Optimizer account and obtain Account ID from
  provided javascript listing. Look for the numeric value set in the variable
  specific to each version at the top of the code:
    - sync: "var _vis_opt_account_id = [account_id]"
    - async: "var account_id=[account_id]"
    - Or copy and paste all of the code into the Account ID Extractor at:
    [yoursite]/admin/config/system/visual_website_optimizer/vwoid

2.  Enter your Account ID and enable the module on the settings page at
  [yoursite]/admin/config/system/visual_website_optimizer

3.  Head to the [VWO website][3] and configure your tests.

4.  ..

5.  Profit!

[3]: https://www.vwo.com/

### Asynchronous vs Synchronous javascript

VWO now highly recommend using the Asynchronous loading of the javascript. The
module now defaults to this mode for all new installs, but existing users will
remain on their current settings.

The setting to change between Synchronous and Asynchronous remains.

Please see the VWO blog posting on the subject:
https://vwo.com/blog/asynchronous-code/

### Credits

This module is based on the work of [Will Ronco of Awesome Software][4],
submitted as a module to node #759278. Updated by Ted Cooper to use most
recent javascript from VWO, prepare for inclusion on Drupal.org, and include a
few additional features. The D8 version was finally instigated after prompting
from IT-Cru with a partial patch. The D9 and D10 versions are by Ted Cooper.
See Git or CHANGELOG.txt for full list of changes.

[4]: http://awesome-software.net/
