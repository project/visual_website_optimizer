# Changelog - VWO Drupal module

### v8.x-1.5, 2025-01-14

- [#3499432] Update SmartCode to v2.1

### v8.x-1.4, 2024-06-11

- [#3435570] Drupal 11 compatibility; Automated Project Update Bot
- [#3453814] Add Gitlab CI testing for linting only
- [#3453814] Fix phpcs and cspell issues

### v8.x-1.3, 2023-12-01

- Change installed default to exclude admin pages.
- Add JS moved from hook_page_attachments to EventSubscriber.
- [#2203267] Add function to allow adding SmartCode to any page.
- [#1920192] Add custom_urls feature.
- [#3186631] Add VWO SmartCode to head.
- Add option to flush caches after changing settings.
- Use local static instead of drupal_static.
- Port missing synchronous support from 7.x-1.x with D8 changes.
- Fix CS in CHANGELOG and README.
- Port missing hook_requirements from 7.x-1.x branch.
- Update composer.json with project information.
- Manual CS fixes.
- CS phpcbf automated fixes.

### v8.x-1.2, 2023-10-10

First D10 version.

- Issue #3374919 by Foxy-vikvik: Drupal 10 Compatibility
- Updated affiliate URL links. Show links only when no account set.

### v8.x-1.1, 2021-06-03

Minimum version bumped to 8.8.0

Removed deprecated code.

Fixed the spelling on the name of the module.

### v8.x-1.1-rc2, 2020-05-24

Drupal 9 deprecated code changes. Work arounds for 8.7.x core needed.

Issue #3142143 by ELC, Project Update Bot: Fix for path.alias_manager rename.

### v8.x-1.1-rc1, 2020-05-15

The VWO Smartcode to include has been changed. Preparing for D9 release.

- Issue #3125820 by Luke.Leber: Fixes for Drupal 9 Deprecated Code
- Issue #3073285: Update Async VWO Smartcode.

### v8.x-1.0, 2018-09-21

It's been 2 years since the alpha was released and people are using it without
complaint; clearly it is time for a full release.

Minor bugfix included which prevented using async include of VWO code.

### v8.x-1.0-alpha1, 2016-09-03

Initial release of D8 version which should allow for generic use of the VWO
code on a Drupal site. This is a slightly feature limited release as a few of
the features added to 7.x-1.x are not available (yet).

There have been large changes to how javascript can be added to a site, with
an outright ban on inline js, plus a move to footer for Drupal Core js. This
prevents the VWO Smart Code from being inserted verbatim by a module, and if
we include it at the top, will cause jQuery to be loaded twice.

The 8.x-1.x branch is based off a direct port of 7.x-1.x branch and will be
updated to use more core functionality.

Changes from 7.x-1.4 version and some notes:

  - Synchronous code not available; default was Asynchronous anyway.
  - All instrcutions and external URLs updated to the new interface and
    terminology.
  - Global Custom URL not available as a setting - probably not that useful
    anyway.
  - Smart Code is added to FOOTER as this is where Drupal 8 adds such
    attachments.
  - The "Smart Code Checker" will FAIL to find the code installed for any
    version.

Thanks to IT-Cru who prompted me to get on with this port.

### v7.x-1.x and before

See version 7.x-1.4 for previous changelog entries:
http://cgit.drupalcode.org/visual_website_optimizer/tree/CHANGELOG.txt?h=7.x-1.4
